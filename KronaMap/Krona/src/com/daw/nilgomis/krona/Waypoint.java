package com.daw.nilgomis.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;

import llibreries.Cadena;

public class Waypoint {
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
	
	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		ComprovacioRendiment comprovacioRendimentTmp = new ComprovacioRendiment();
		
		return comprovacioRendimentTmp;
	}
	
	public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear,
		ComprovacioRendiment comprovacioRendimentTmp) {
		
		long inici;
		long fi;
		
		
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		
	
		System.out.println("ArrayList");
		inici = System.nanoTime();
		
		for(int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaArrayList.add(new Waypoint_Dades(0,"Orbita de la Terra",new int[] {0,0,0},
					true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null ));			
			
		}
		
		fi = System.nanoTime();
		
		long tempsNano = fi-inici;
	
		long tempsMili = TimeUnit.MILLISECONDS.convert(tempsNano, TimeUnit.NANOSECONDS);
		
		System.out.println(numObjACrear + " Waypoints han trigat "+ tempsMili +" milisegons en afegir-se a l'ArrayList");
		
		
		System.out.println("LinkedList");
		
		inici = System.nanoTime();
		
		for(int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaLinkedList.add(new Waypoint_Dades(0,"Orbita de la Terra",new int[] {0,0,0},
					true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null ));			
			
		}
		
		fi = System.nanoTime();
		
		tempsNano = fi-inici;
	
		tempsMili = TimeUnit.MILLISECONDS.convert(tempsNano, TimeUnit.NANOSECONDS);
		
		System.out.println(numObjACrear + " Waypoints han trigat "+ tempsMili +" milisegons en afegir-se a la LinkedList");
		
		return comprovacioRendimentTmp;
	
	}
	
	public static ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment
			comprovacioRendimentTmp) {
		
		int meitatLlista;
		
		meitatLlista = comprovacioRendimentTmp.llistaArrayList.size()/2;
		
		long inici;
		long fi;
		
				
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		
		
		System.out.println("llistaArrayList.size(): " + comprovacioRendimentTmp.llistaArrayList.size()+" meitat de la llista = " + meitatLlista);
		
		inici = System.nanoTime();
		
		comprovacioRendimentTmp.llistaArrayList.add(new Waypoint_Dades(0,"Orbita de la Terra",new int[] {0,0,0},
					true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null ));			
			
				
		fi = System.nanoTime();
		
		long tempsNano = fi-inici;
	
		System.out.println("Temps per a insertat 1 waypoint en la posicio 1 de l'ArrayList: " + tempsNano );
		
				
		
		inici = System.nanoTime();
		
		comprovacioRendimentTmp.llistaLinkedList.add(new Waypoint_Dades(0,"Orbita de la Terra",new int[] {0,0,0},
					true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null ));			
			
				
		fi = System.nanoTime();
		
		tempsNano = fi-inici;
	

		
		System.out.println("Temps per a insertat 1 waypoint en la posicio 1 de la LinkedList: " + tempsNano );
		
		System.out.println("------------------------");
		
		
		inici = System.nanoTime();
		
		comprovacioRendimentTmp.llistaArrayList.add(new Waypoint_Dades(meitatLlista,"Orbita de la Terra",new int[] {0,0,0},
					true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null ));			
			
				
		fi = System.nanoTime();
		
		tempsNano = fi-inici;
	
		
		
		System.out.println("Temps per a insertat 1 waypoint en el mig de l'ArrayList: " + tempsNano );
		
		
		inici = System.nanoTime();
		
		comprovacioRendimentTmp.llistaLinkedList.add(new Waypoint_Dades(meitatLlista,"Orbita de la Terra",new int[] {0,0,0},
					true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null ));			
			
				
		fi = System.nanoTime();
		
		tempsNano = fi-inici;
	
		
		
		System.out.println("Temps per a insertat 1 waypoint en el mig de la LinkedList: " + tempsNano );
		
		System.out.println("------------------------");
		
		
		inici = System.nanoTime();
		
		comprovacioRendimentTmp.llistaArrayList.add(new Waypoint_Dades(comprovacioRendimentTmp.llistaLinkedList.size(),"Orbita de la Terra",new int[] {0,0,0},
					true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null ));			
			
				
		fi = System.nanoTime();
		
		tempsNano = fi-inici;
	
		
		System.out.println("Temps per a insertat 1 waypoint al final de l'ArrayList: " + tempsNano );
		
			
		inici = System.nanoTime();
		
		comprovacioRendimentTmp.llistaLinkedList.add(new Waypoint_Dades(comprovacioRendimentTmp.llistaLinkedList.size(),"Orbita de la Terra",new int[] {0,0,0},
					true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null ));			
			
				
		fi = System.nanoTime();
		
		tempsNano = fi-inici;
	
				
		System.out.println("Temps per a insertat 1 waypoint al final de la LinkedList: " + tempsNano );
		
		System.out.println("------------------------");
		
		return comprovacioRendimentTmp;
		
		
				
	}
	




	public static ComprovacioRendiment modificarWaypoints(ComprovacioRendiment
			comprovacioRendimentTmp) {
		
		List<Integer> idsPerArrayList = new ArrayList<Integer>(10);
		
		for(int i = 0; i < 10; i++) {
			idsPerArrayList.add(i);
			
		}
										
		System.out.println("---- Apartat 1 ----");
		System.out.println("S'ha inicialitzat la llista idsPerArrayList amb 10 elements.");
		System.out.println("El primer element te el valor: 0");
		System.out.println("L'ultim element te el valor: " + ((idsPerArrayList.size())-1));
		
		System.out.println("---- Apartat 2 ----");
				
		
		for(int element : idsPerArrayList) {
			
			int id = element;
			
			System.out.println("ABANS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get(" + id +").get(0): " + comprovacioRendimentTmp.llistaArrayList.get(id).getId());
			
			comprovacioRendimentTmp.llistaArrayList.get(id).setId(id);
			
			System.out.println("ABANS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get("+ id +").get(0): " + comprovacioRendimentTmp.llistaArrayList.get(id).getId());
			System.out.println();
			
			
		} 
		
		System.out.println("---- Apartat 3.1 (Bucle For) ----");
				
		
		for(Waypoint_Dades element : comprovacioRendimentTmp.llistaArrayList) {
			System.out.println("ID = " + element.getId() + ", nom = " + element.getNom());		
					
		} 
		
		System.out.println("---- Apartat 3.1 (iterator) ----");
				
		
		 ListIterator<Waypoint_Dades> 
         iterator = comprovacioRendimentTmp.llistaArrayList.listIterator(); 

	     while (iterator.hasNext()) { 
	    	 Waypoint_Dades next = iterator.next();
	    	 System.out.println("ID = " + next.getId() + ", nom = " + next.getNom());	
	     } 
			
	     System.out.println("---- Apartat 4 ----");
	     System.out.println("Preparat per esborrar el contingut de llistaLinkedList que te 10 elements");
	     comprovacioRendimentTmp.llistaLinkedList.clear();
	     System.out.println("Esborrada. Ara llistalinkedList te 0 elements.");
	     for(Waypoint_Dades element : comprovacioRendimentTmp.llistaArrayList) {
	    	 comprovacioRendimentTmp.llistaLinkedList.add(element);	
						
			} 
	     System.out.println("Copiats. Ara llistalinkedList te "+comprovacioRendimentTmp.llistaLinkedList.size() +" elements.");
	     System.out.println();
	     
	     System.out.println("---- Apartat 5 ----");
	     System.out.println("---- Apartat 5.1 bucle for ----");
	     for(Waypoint_Dades element : comprovacioRendimentTmp.llistaArrayList) {
	    	 if(element.getId()>5) {
	    	 element.setNom("Orbita de Mart");	
	    	 System.out.println("Modificat el waipoint amb id = " + element.getId());
	    	 }
	     } 
	     System.out.println("---- Apartat 5.1 comprovacio ----");
	     for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
	     System.out.println("El waypoint amb id = "+comprovacioRendimentTmp.llistaArrayList.get(i).getId()  +" te el nom " +
	    		 			comprovacioRendimentTmp.llistaArrayList.get(i).getNom());
	     }
	     
	     System.out.println("---- Apartat 5.2 bucle for ----");
	     for(Waypoint_Dades element : comprovacioRendimentTmp.llistaLinkedList) {
	    	 if(element.getId()<5) {
	    	 element.setNom("Punt Lagrange entre la Terra i la LLuna");	
	    	 System.out.println("Modificat el waipoint amb id = " + element.getId());
	    	 }
	     } 
	     System.out.println("---- Apartat 5.2 comprovacio ----");
	     for (Waypoint_Dades element : comprovacioRendimentTmp.llistaLinkedList) {
	     System.out.println("El waypoint amb id = "+element.getId()  +" te el nom " +
	    		 element.getNom());
	     }
	     
	     
		return comprovacioRendimentTmp;
		
		
		
		
		
		
	}
	
	public static ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment
			comprovacioRendimentTmp) {
		
		/*for(Waypoint_Dades element : comprovacioRendimentTmp.llistaArrayList) {
	    	 if (element.getId() < 6 ) {
	    		 comprovacioRendimentTmp.llistaArrayList.remove(element);
	    	 }						
			}
		
		*/
		System.out.println("---- Apartat 1 ----");
		System.out.println("No deixa esborrar amb el for each de tal forma que per tal de poder esborrar un element el qual has recorregut en un iterador s'ha de fer servir el java iterator");
		System.out.println("---- Apartat 2 Iterator ----");
		ListIterator<Waypoint_Dades> 
        iterator = comprovacioRendimentTmp.llistaLinkedList.listIterator(); 
        
	     while (iterator.hasNext()) { 
	    	 Waypoint_Dades next = iterator.next();
	    	 if(next.getId() > 4) {
	    	 iterator.remove();
	    	 System.out.println("Esborrat el waypoint amb id = " + next.getId());	
	    	 }
	     } 
	     
		System.out.println("---- Apartat 2 Comprovacio ----");
	     for(Waypoint_Dades element : comprovacioRendimentTmp.llistaLinkedList) {
	    		System.out.println("El waypoint amb id = "+ element.getId()+" te el nom " + element.getNom());
	    	 }						
			
	     System.out.println("---- Apartat 3 list iterator ----");
	     
	     ListIterator<Waypoint_Dades> 
	        iterator2 = comprovacioRendimentTmp.llistaLinkedList.listIterator(); 
	     
	     while (iterator2.hasNext()) { 
	    	 Waypoint_Dades next = iterator2.next();
	    	 if(next.getId() == 2 ) {
	    	 iterator2.remove();
	    	 System.out.println("Esborrat el waypoint amb id = " + next.getId());	
	    	 }
	     } 
	     
	     
	     System.out.println("---- Apartat 3 comprovacio ----");
	     while (iterator2.hasPrevious()) { 
	    	 Waypoint_Dades previous = iterator2.previous();
	    	System.out.println("El waypoint amb id = "+ previous.getId()+" te el nom " + previous.getNom());
	    	 }
	     
	     
		
		return comprovacioRendimentTmp;
		
	}
	
	public static ComprovacioRendiment modificarCoordenadesINomDeWaypoints(ComprovacioRendiment
			comprovacioRendimentTmp) {
		
			
		int i, j, stringCoordinatesLength;
		String name, newName, coordinates, stringNewCoordinates;
		String[] newStringArrayCoordinates;
		int[] newIntArrayCoordinates;
		Scanner s = new Scanner(System.in);
		for (i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			Waypoint_Dades waypoint = comprovacioRendimentTmp.llistaArrayList.get(i);
			if (waypoint.getId() % 2 == 0) {
				System.out.println("------ Modificar el waypoint amb id = " + waypoint.getId() + " ------");
				name = waypoint.getNom();
				System.out.println("Nom actual: " + name);
				System.out.print("Nom nou: ");
				newName = s.nextLine();
				waypoint.setNom(newName);
				coordinates = waypoint.getCoordenades()[0] + " " + waypoint.getCoordenades()[1] + " "
						+ waypoint.getCoordenades()[2];
				System.out.println("Coordenades actuals: " + coordinates);
				System.out.print("Coordenades noves (format: 1 13 7): ");
				stringNewCoordinates = s.nextLine();
				stringCoordinatesLength = stringNewCoordinates.split(" ").length;
				stringCoordinatesLength = stringNewCoordinates.split(" ").length;
				newIntArrayCoordinates = new int[waypoint.getCoordenades().length];
				while (stringCoordinatesLength != 3) {
					if (stringCoordinatesLength != -1 ) {
						System.out.println("ERROR: introdu�r 3 par�metres separats per 1 espai en blanc. Has introdu�t "
								+ stringCoordinatesLength + " par�metres.");
					}
					System.out.println("Coordenades actuals: " + coordinates);
					System.out.print("Coordenades noves (format: 1 13 7): ");
					stringNewCoordinates = s.nextLine();
					stringCoordinatesLength = stringNewCoordinates.split(" ").length;
					if (stringCoordinatesLength == 3) {
						newIntArrayCoordinates = new int[stringCoordinatesLength];
						newStringArrayCoordinates = stringNewCoordinates.split(" ");
						for (j = 0; j < stringCoordinatesLength; j++) {
							if (Cadena.stringIsInt(newStringArrayCoordinates[j])) {
								
								newIntArrayCoordinates[j] = Integer.parseInt(newStringArrayCoordinates[j]);
							} else {
								
								System.out.println("ERROR: coordenada " + newStringArrayCoordinates[j] + " no v�lida.");
								stringCoordinatesLength = -1;
								break;
							}
						}
					}
				}
				waypoint.setCoordenades(newIntArrayCoordinates);
				System.out.println();
			}
		}
		return comprovacioRendimentTmp;
		
	}
	
	public static void visualitzarWaypointsOrdenats(ComprovacioRendiment
			comprovacioRendimentTmp) {
		
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++ ) {
			System.out.println(comprovacioRendimentTmp.llistaArrayList.get(i));			
		}
		
	}
	
	public static void waypointsACertaDistanciaMaxDeLaTerra(ComprovacioRendiment
			comprovacioRendimentTmp) {
		System.out.println("Distancia maxima a la terra ");
		
		Scanner s = new Scanner(System.in);
		
		String dist = s.nextLine();
		
		while(Cadena.stringIsInt(dist) == false) {
			System.out.println("Torna a posar la distancia maxima a la terra ");
			dist = s.nextLine();
		}
		int coords = 0;
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++ ) {
			
						
			for(int j = 0; j < 3; j++) {
				
				coords = coords + ((comprovacioRendimentTmp.llistaArrayList.get(i).getCoordenades()[j])*(comprovacioRendimentTmp.llistaArrayList.get(i).getCoordenades()[j]));
				System.out.println(comprovacioRendimentTmp.llistaArrayList.get(i).getCoordenades()[j]);
				
			}

			int distancia = Integer.parseInt(dist);
			if( coords <= distancia) {
				System.out.println(comprovacioRendimentTmp.llistaArrayList.get(i));	
				System.out.println(coords);
			}
			
						
		}
		
		
	}

	
	



}

package com.daw.nilgomis.krona;

import java.time.LocalDateTime;

import varies.Data;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeSet;

import llibreries.Cadena;



public class Ruta {
	
	
	public static List<Waypoint_Dades> crearRutaInicial() {
		List<Waypoint_Dades> llistaWaypointLinkedList = null;
		
		
		llistaWaypointLinkedList = new LinkedList<Waypoint_Dades>();
		
		llistaWaypointLinkedList.add(new Waypoint_Dades(0, "�rbita de la Terra", new int[] {0,0,0}, true, LocalDateTime.parse("21-10-2020 01:10", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(1, "Punt Lagrange Terra-LLuna", new int[] {1,1,1}, true, LocalDateTime.parse("21-10-2020 01:00", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(2, "�rbita de la LLuna", new int[] {2,2,2}, true, LocalDateTime.parse("21-10-2020 00:50", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(3, "�rbita de Mart", new int[] {3,3,3}, true, LocalDateTime.parse("21-10-2020 00:40", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(4, "�rbita de J�piter", new int[] {4,4,4}, true, LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(5, "Punt Lagrange J�piter-Europa", new int[] {5,5,5}, true, LocalDateTime.parse("21-10-2020 00:20", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(6, "�rbita de Europa", new int[] {6,6,6}, true, LocalDateTime.parse("21-10-2020 00:10", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(7, "�rbita de Venus", new int[] {7,7,7}, true, LocalDateTime.parse("21-10-2020 00:01", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		
		return llistaWaypointLinkedList;
	}
	
	
	public static ComprovacioRendiment inicialitzarRuta(ComprovacioRendiment
			comprovacioRendimentTmp) {
		
		crearRutaInicial();
		
		for(int i = 0; i < crearRutaInicial().size(); i++) {
			comprovacioRendimentTmp.pilaWaypoints.push(crearRutaInicial().get(i));	
			
			
		}
		
		
		
		Waypoint_Dades d = new Waypoint_Dades(4, "�rbita de J�piter REPETIDA", new int[] {4,4,4}, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter));
		comprovacioRendimentTmp.pilaWaypoints.add(d);
		
		comprovacioRendimentTmp.wtmp = d;
		
		
		return comprovacioRendimentTmp;
		
		
	}
	
	public static void visualitzarRuta(ComprovacioRendiment comprovacioRendimentTmp){
		for (Waypoint_Dades element : comprovacioRendimentTmp.pilaWaypoints) 
        { 
            System.out.println(element); 
        } 
	}
	
	public static void invertirRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		  
	      Deque<Waypoint_Dades> pilaWaypointsInversa = new ArrayDeque<Waypoint_Dades>();
	      
	      for (Waypoint_Dades element : comprovacioRendimentTmp.pilaWaypoints) 
	        { 
	    	  pilaWaypointsInversa.push(element);
	        } 
	      
	      for (Waypoint_Dades element : pilaWaypointsInversa) 
	        { 
	    	  System.out.println(element);
	        } 
	}
	
	public static void existeixWaypointEnRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		
	
		if (comprovacioRendimentTmp.pilaWaypoints.contains(comprovacioRendimentTmp.wtmp)) {
				System.out.println("Si s'ha trobat");
			} else {
				
				System.out.println("No s'ha trobat");
				comprovacioRendimentTmp.wtmp = new Waypoint_Dades(4, "�rbita de J�piter REPETIDA", new int[] {4,4,4}, true,
						LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
						LocalDateTime.parse("22-10-2020 23:55", Data.formatter));
			}
	}
	
	public static ComprovacioRendiment inicialitzaLListaRutes(ComprovacioRendiment
			comprovacioRendimentTmp) {
		
		Ruta_Dades ruta_0 = new Ruta_Dades(0, "ruta 0: Terra --> Punt Lagrange J�piter-Europa", new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5)), true, LocalDateTime.parse("28-10-2020 16:30", Data.formatter), null, LocalDateTime.parse("28-10-2020 16:30", Data.formatter));
		
		Ruta_Dades ruta_1 = new Ruta_Dades(1, "ruta 1: Terra --> �rbita de Mart (directe)", new
				ArrayList<Integer>(Arrays.asList(0, 3)), true, LocalDateTime.parse("28-10-2020 16:31",
						Data.formatter), null, LocalDateTime.parse("28-10-2020 16:31", Data.formatter));
		
		Ruta_Dades ruta_2 = new Ruta_Dades(2, "ruta 2.1: Terra --> �rbita de Venus", new
				ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true, LocalDateTime.parse("28-10-2020 16:32", Data.formatter), null, LocalDateTime.parse("28-10-2020 16:32",
						Data.formatter));
		
		Ruta_Dades ruta_3 = new Ruta_Dades(3, "ruta 3: Terra --> Mart (directe) --> �rbita de J�piter ", new
				ArrayList<Integer>(Arrays.asList(0, 3, 4)), true, LocalDateTime.parse("28-10-2020 16:33",
						Data.formatter), null, LocalDateTime.parse("28-10-2020 16:33", Data.formatter));
		
		Ruta_Dades ruta_4 = new Ruta_Dades(4, "ruta 2.2: Terra --> �rbita de Venus (REPETIDA)", new
				ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true, LocalDateTime.parse("28-10-2020 16:32", Data.formatter), null, LocalDateTime.parse("30-10-2020 19:49",
						Data.formatter));
		
		comprovacioRendimentTmp.llistaRutes.add(ruta_0);
		comprovacioRendimentTmp.llistaRutes.add(ruta_1);
		comprovacioRendimentTmp.llistaRutes.add(ruta_2);
		comprovacioRendimentTmp.llistaRutes.add(ruta_3);
		comprovacioRendimentTmp.llistaRutes.add(ruta_4);
		
		for (Ruta_Dades ruta : comprovacioRendimentTmp.llistaRutes) {
			System.out.println(ruta.getNom() + ": waypoints" + ruta.getWaypoints());
		}

		return comprovacioRendimentTmp;
		
		
	}
	
	public static void setUnio(ComprovacioRendiment comprovacioRendimentTmp) {
		
		Set<Integer> waypoints = new HashSet<Integer>();
		for (Ruta_Dades ruta : comprovacioRendimentTmp.llistaRutes) {
			waypoints.addAll(ruta.getWaypoints());
		}
		
		System.out.println("ID dels waypoints ficats en el set: " + waypoints);

		
	}
	
	public static void setInterseccio(ComprovacioRendiment comprovacioRendimentTmp) {
		Set<Integer> interseccio = new HashSet<Integer>();
		//afegim tots els waypoints
		for(Ruta_Dades rutaTemporal : comprovacioRendimentTmp.llistaRutes) {
			interseccio.addAll(rutaTemporal.getWaypoints());
		}
		//solament retenim els que estan en totes les rutes
		for(Ruta_Dades rutaTemporal : comprovacioRendimentTmp.llistaRutes) {
			interseccio.retainAll(rutaTemporal.getWaypoints());
			
		}
		
		System.out.println("ID dels waypoints en totes les rutes: " + interseccio);
		
	}
	
	private static int buscarRuta(int numRuta, ComprovacioRendiment comprovacioRendimentTmp) {
		
		int var= -1;
		for(int i = 0; i <  comprovacioRendimentTmp.llistaRutes.size(); i++) {
			if(comprovacioRendimentTmp.llistaRutes.get(i).getId()==numRuta) {
				var = comprovacioRendimentTmp.llistaRutes.get(i).getId();
				
			}	
			
		}	
		
		return var;
	}
	
	public static void setResta(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Rutes actuals:");
		for(Ruta_Dades rutaTmp2 : comprovacioRendimentTmp.llistaRutes) {
			System.out.println("ID " + rutaTmp2.getId() + ": " + rutaTmp2.getNom() + ": waypoints" + rutaTmp2.getWaypoints());
		}
		
		boolean comprovacio = true;
		String rutes = "";
		int parametres;
		StringTokenizer st;
		String rutaTmp = "";
		int rutaA = 0;
		int rutaB = 0;
		int posRutaA;
		int posRutaB;
		Set<Integer> resta = new HashSet<Integer>();
		do {
			comprovacio = false;
			
			System.out.print("Selecciona ruta A i B (format: 3 17): ");
			rutes = sc.nextLine();
			
			parametres = rutes.split(" ").length;
			if (parametres != 2) {
				System.out.println("ERROR: introduir 2 par�metres separats per 1 espai en blanc. Has introduit " + parametres + " par�metres.");
				comprovacio = true;
			}
			
			if (!comprovacio) {
				
				st = new StringTokenizer(rutes, " ");
				rutaTmp = st.nextToken();
				//fem la comprovacio del tipus de variable per la ruta A
				if (Cadena.stringIsInt(rutaTmp)) {
					rutaA = Integer.parseInt(rutaTmp);
				} else {
					System.out.println("ERROR: has introduit " + rutaTmp + " com a ruta. Els ID de les rutes s�n integers.");
					comprovacio = true;
				}
				//fem la comprovacio del tipus de variable per la ruta B
				if (!comprovacio) {
					rutaTmp = st.nextToken();
					
					if (Cadena.stringIsInt(rutaTmp)) {
						rutaB = Integer.parseInt(rutaTmp);
					} else {
						System.out.println("ERROR: has introduit " + rutaTmp + " com a ruta. Els ID de les rutes s�n integers.");
						comprovacio = true;
					}
					
					if (!comprovacio) {
						posRutaA = buscarRuta(rutaA, comprovacioRendimentTmp);
						if (posRutaA != -1) {
							posRutaB = buscarRuta(rutaB, comprovacioRendimentTmp);
							if (posRutaB != -1) {
								
								System.out.println();
								// Un cop tenim ja les dues rutes correctes mirem quins waypoints tenim a la A pero no a la B
								resta.addAll(comprovacioRendimentTmp.llistaRutes.get(posRutaA).getWaypoints());
								System.out.println("HashSet (havent-hi afegir els waypoints de la ruta A) = " + resta);
								
								resta.removeAll(comprovacioRendimentTmp.llistaRutes.get(posRutaB).getWaypoints());
								System.out.println("HashSet (havent-hi tret els waypoints de la ruta B) = " + resta);
							} else {
								System.out.println("ERROR: no existeix la ruta " + rutaB + " en el sistema.");
								comprovacio = true;
							}
						} else {
							System.out.println("ERROR: no existeix la ruta " + rutaA + " en el sistema.");
							comprovacio = true;
						}
					}
				}
			}
		} while (comprovacio);
	}

	
	public static void crearSetOrdenatDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		SortedSet<Ruta_Dades> ordenat = new TreeSet<Ruta_Dades>();
		
		
		ordenat.addAll(comprovacioRendimentTmp.llistaRutes);
				
		for(Ruta_Dades ruta : ordenat) {
			System.out.println("ID " + ruta.getId() + ": " + ruta.getNom() + ": waypoints" + ruta.getWaypoints());
		}
	}
	
	public static void crearLinkedHashMapDeRutes (ComprovacioRendiment
			comprovacioRendimentTmp) {
		
		int key;
		int forma1;
		int forma2;
		int forma3;
		LinkedHashMap<Integer, Ruta_Dades> rutes;
		

		rutes = new LinkedHashMap<Integer, Ruta_Dades>();
		
		for (Ruta_Dades ruta : comprovacioRendimentTmp.llistaRutes) {
			rutes.put(ruta.getId(), ruta);
		}
		
		//Forma 1
		
		long tempsInicial = System.nanoTime();
		
		
		Set set = rutes.entrySet();
		Iterator it1 = set.iterator();
		System.out.println("1a forma de visualitzar el contingut del map (map --> set + iterador del set):");
		while (it1.hasNext()) {
			Map.Entry entry = (Map.Entry) it1.next();
			System.out.println("Clau del map = " + entry.getKey() + ": \n" + entry.getValue().toString());
		}
		long tempsFinal = System.nanoTime();
		
		forma1 = (int) ((tempsFinal - tempsInicial) / 1000);

		//Forma 2
		
		tempsInicial = System.nanoTime();
		System.out.println();
		System.out.println("2a forma de visualitzar el contingut del map (iterator de les claus del map):");
		Iterator<Integer> it2 = rutes.keySet().iterator();
		while (it2.hasNext()) {
			key = it2.next();
			System.out.println(key + ": " + rutes.get(key));
		}
		tempsFinal = System.nanoTime();
		forma2 = (int) ((tempsFinal - tempsInicial) / 1000);
		
		// Forma 3
		
		tempsInicial = System.nanoTime();
		System.out.println();
		System.out.println("3a forma de visualitzar el contingut del map (for-each del map --> set):");
		for (Entry<Integer, Ruta_Dades> r : rutes.entrySet()) {
			System.out.println(r.getKey() + ": " + r.getValue().toString());
		}
		tempsFinal = System.nanoTime();
		forma3 = (int) ((tempsFinal - tempsInicial) / 1000);

		System.out.println();
		System.out.println("TEMPS PER 1a FORMA (map --> set + iterador del set): " + forma1);
		System.out.println("TEMPS PER 2a FORMA (iterator de les claus del map): " + forma2);
		System.out.println("TEMPS PER 3a FORMA (for-each del map --> set): " + forma3);

		comprovacioRendimentTmp.mapaLinkedDeRutes.putAll(rutes);

	}
	
	public static void visualitzarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment
			comprovacioRendimentTmp) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Escriu el n� del waypoint que vols buscar:");
		String waypoints = sc.nextLine();
		boolean comprovacio = false;
		
		if (!Cadena.stringIsInt(waypoints)) {
			System.out
					.println("ERROR: has introduit " + waypoints + " com a ruta. Els ID de les rutes s�n integers.");
		} else {
			comprovacio = true;
		}
		// Ara mriem que sigui un enter seguint una forma molt similar a la del exercici de setResta
		while (!comprovacio) {
			System.out.print("Escriu el n� del waypoint que vols buscar:");
			waypoints = sc.nextLine();
			if (!Cadena.stringIsInt(waypoints)) {
				System.out.println(
						"ERROR: has introduit " + waypoints + " com a ruta. Els ID de les rutes s�n integers.");
			} else {
				comprovacio = true;
			}
		}
		// Ara es reccorre
		if (comprovacio) {
			// S'ha de parsejar
			int waypointToInt = Integer.parseInt(waypoints);
			for (Entry<Integer, Ruta_Dades> ruta : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
				// Si conte el id en questio ho mostrem
				if (ruta.getValue().getWaypoints().contains(waypointToInt)) {
					System.out.println(ruta.getKey() + ": " + ruta.getValue().toString());
				}
			}
		}

		
		
		
	}
	
	public static void esborrarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment
			comprovacioRendimentTmp) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Escriu el n� del waypoint que vols buscar:");
		String waypoints = sc.nextLine();
		boolean comprovacio = false;
		
		if (!Cadena.stringIsInt(waypoints)) {
			System.out
					.println("ERROR: has introduit " + waypoints + " com a ruta. Els ID de les rutes s�n integers.");
		} else {
			comprovacio = true;
		}
		// Ara mriem que sigui un enter seguint una forma molt similar a la del exercici de setResta
		while (!comprovacio) {
			System.out.print("Escriu el n� del waypoint que vols buscar:");
			waypoints = sc.nextLine();
			if (!Cadena.stringIsInt(waypoints)) {
				System.out.println(
						"ERROR: has introduit " + waypoints + " com a ruta. Els ID de les rutes s�n integers.");
			} else {
				comprovacio = true;
			}
		}
		
		if (comprovacio) {
			// S'ha de parsejar
			int waypointToInt = Integer.parseInt(waypoints);
			
			Iterator<Integer> iterator = comprovacioRendimentTmp.mapaLinkedDeRutes.keySet().iterator();
			while (iterator.hasNext()) {
				int key = iterator.next();
				Ruta_Dades ruta = comprovacioRendimentTmp.mapaLinkedDeRutes.get(key);
				// Es mira si esta contingut
				if (ruta.getWaypoints().contains(waypointToInt)) {
					// Passem a eleminar-lo
					System.out.println(ruta.toString());
					iterator.remove();
				}
			}
		}

	}
	
	public static void visualitzarUnaRutaDelMap(ComprovacioRendiment comprovacioRendimentTmp) {
		
	}

	
	
	
	

}

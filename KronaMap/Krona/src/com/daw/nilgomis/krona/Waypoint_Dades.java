package com.daw.nilgomis.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;



public class Waypoint_Dades implements Comparable<Waypoint_Dades> {
	private int id;                     
	private String nom;
	private int[] coordenades;
	private boolean actiu;              
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;      
	private LocalDateTime dataModificacio;
    
	public Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		
		this.id = id;
		this.nom = nom;
		this.coordenades = coordenades;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int[] getCoordenades() {
		return coordenades;
	}

	public void setCoordenades(int[] coordenades) {
		this.coordenades = coordenades;
	}

	public boolean isActiu() {
		return actiu;
	}

	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}

	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}

	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	public LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}

	public void setDataAnulacio(LocalDateTime dataAnulacio) {
		this.dataAnulacio = dataAnulacio;
	}

	public LocalDateTime getDataModificacio() {
		return dataModificacio;
	}

	public void setDataModificacio(LocalDateTime dataModificacio) {
		this.dataModificacio = dataModificacio;
	}
	

	
	
	@Override
	public int compareTo(Waypoint_Dades o) {
		int flag = 0;
		for(int i = 0; i < o.coordenades.length; i++) {
			if(o.coordenades[i] == this.coordenades[i]) {
				flag++;
			}
		}
		if(flag ==3) {
		return this.getNom().compareTo(o.getNom());
		}
		
		int cuadrat1 = 0;
		int cuadrat2 = 0;
		
		for(int i = 0; i < o.coordenades.length; i++) {
			cuadrat1 = cuadrat1 + (this.coordenades[i]*this.coordenades[i]);
			cuadrat1 = cuadrat1 + (o.coordenades[i]*o.coordenades[i]);
		}
		
		return cuadrat2 - cuadrat1;
		
	}

	@Override
	public String toString() {
		return "Waypoint " + this.getId() + "\n\t\t"
				+ "Nom = " + this.getNom() + "\n\t\t"
				+ "coordenades = " + this.getCoordenades() + "\n\t\t" 
				+ "actiu = " + this.isActiu() + "\n\t\t" 
				+ "data creacio = " + this.getDataCreacio() + "\n\t\t" 
				+ "data anulacio = " + this.getDataAnulacio() + "\n\t\t"
				+ "data modificacio = " + this.getDataModificacio() + "\n\t\t";
	}
	
	
	   
    
}

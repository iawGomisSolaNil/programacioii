package com.daw.nilgomis.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Ruta_Dades implements Comparable<Ruta_Dades>{
	private int id;                             	
	private String nom;
	private ArrayList<Integer> waypoints;       	
	private boolean actiu;                      	
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;             
	private LocalDateTime dataModificacio;
    
	

	public Ruta_Dades(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Integer> getWaypoints() {
		return waypoints;
	}

	public void setWaypoints(ArrayList<Integer> waypoints) {
		this.waypoints = waypoints;
	}

	public boolean isActiu() {
		return actiu;
	}

	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}

	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}

	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	public LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}

	public void setDataAnulacio(LocalDateTime dataAnulacio) {
		this.dataAnulacio = dataAnulacio;
	}

	public LocalDateTime getDataModificacio() {
		return dataModificacio;
	}

	public void setDataModificacio(LocalDateTime dataModificacio) {
		this.dataModificacio = dataModificacio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((waypoints == null) ? 0 : waypoints.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ruta_Dades other = (Ruta_Dades) obj;
		if (waypoints == null) {
			if (other.waypoints != null)
				return false;
		} else if (!waypoints.equals(other.waypoints))
			return false;
		return true;
	}

	
	


	@Override
	public String toString() {
		return "Ruta_Dades [id=" + id + ", nom=" + nom + ", waypoints=" + waypoints + ", actiu=" + actiu
				+ ", dataCreacio=" + dataCreacio + ", dataAnulacio=" + dataAnulacio + ", dataModificacio="
				+ dataModificacio + "]";
	}
	
	@Override
	public int compareTo(Ruta_Dades route) {
		return route.getId() - this.id;
	}

	
	
    
    
}

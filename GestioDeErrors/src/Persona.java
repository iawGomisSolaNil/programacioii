
public class Persona {
	
	private int edat;

	public Persona(int edat) {
		this.edat = edat;
	}
	
	
		
	//SETTER PER L'EXERCICI 4
	
	
	public void setEdat(int edat) throws IllegalArgumentException {
		try {
			if (edat < 0) {
				throw new IllegalArgumentException();
			} else {
				this.edat = edat;
			}
		} catch (IllegalArgumentException e) {
			System.out.println(e);
			System.out.println("L'esdad no pot ser negativa");
		}
	}
	
	
	//SETTER PER L'EXERCICI 5
	
	/*
	public void setEdat(int edat) throws Exercici5ValidarEdatException {
		try {
			if (edat < 0 || edat > 100 ) {
				throw new Exercici5ValidarEdatException();
			} else {
				this.edat = edat;
			}
		} catch (Exercici5ValidarEdatException e) {
			System.out.println(e);
		}
	}

	*/
	
	public int getEdad() {
		return this.edat;
	}



	
}

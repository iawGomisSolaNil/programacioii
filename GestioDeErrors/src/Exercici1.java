public class Exercici1 {

	public static void main(String[] args) {
		
		// Codi que ens donara l'error ArrayIndexOutOfBounds
		
		/*
		int[] a = {1}; System.out.println(a[1]);
		System.out.println("Final del programa!");
		*/
		
		// El missatge no surtira perque hi ha un error en abans 
		
		funcio1();
	
}		
	public static void funcio3() {	
		System.out.println("Executo la funcio 3");
		
		try {
			int[] a = {1}; System.out.println(a[1]);
			
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e);
		} finally {
			System.out.println("Final del programa!"); 
		}

	
		
	}
	
	public static void funcio1() {
		System.out.println("Executo la funcio 1");
		funcio2();
		System.out.println("Sha executat la funcio 2 dins de la funcio 1 i per consequent la funcio 3 dins de la 2. De forma que aquest ha de ser l'ultim missatge que surti per pantalla ");
		
		
	}
	
	public static void funcio2() {
		System.out.println("Executo la funcio 2");
	
		try {
			String a = "";
			System.out.println(a.charAt(1));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}
		
		funcio3();
		
	}
}
	

		
		
		
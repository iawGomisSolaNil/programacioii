
public class Exercici3 {

	
	public static Exception divisio() {
		Exception e = new Exception("Error: el divisor no pot ser 0");
		return e;
	}

	
	public static void main(String[] args) {
		try {
			System.out.println((2 / 0));
		} catch (ArithmeticException e) {
			System.out.println(divisio());
		}
	}


}

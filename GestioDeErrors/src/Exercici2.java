import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Exercici2 {
	
	public static void main(String[] args) {
		
		try {
			fileThrowMethod();
		} catch (FileNotFoundException e) {
			System.out.println("Catch de FileNotFoundException");
			System.out.println(e);
		} catch (IOException e) {
			System.out.println("Catch de IOException");
			System.out.println(e);
		}
	}




	private static void fileThrowMethod() throws FileNotFoundException, IOException{
		FileOutputStream f = new FileOutputStream ("../docs/test.txt");
		f.close();
		
	}
	
}

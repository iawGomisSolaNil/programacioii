package streams;

import java.io.File;
import java.util.ArrayList;


public class Ex9 {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		File dir= new File("./");
		ArrayList<String> newlist = new ArrayList<String>();
		newlist = mostrarFitxersDirectoris(list, dir);
		System.out.println(newlist);
		
	}

	public static ArrayList<String> mostrarFitxersDirectoris(ArrayList<String> list, File dir) {
		ArrayList<String> newlist = new ArrayList<String>();
		for (int i = 0; i < dir.list().length; i++) {
			
			File file = new File( dir.getPath() +"/" + dir.list()[i]);
			if (file.isDirectory()) {
				mostrarFitxersDirectoris(list,file);
			} else {
				newlist.add(dir.list()[i]);
			}
		}
		
		return newlist;
	}
	
}

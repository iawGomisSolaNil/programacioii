package streams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ex5 {

	public static void main(String[] args) throws IOException {
		File file = new File("src/streams");
		
		List<String> names = new ArrayList<String>();
		names = Arrays.asList(file.list());
		FileReader fr = null;
		BufferedReader br;
		String line;
		
		for (String filee : names) {
			int linecont = 0;
			int charcont = 0;
			try {
			
			fr = new FileReader(filee);
			br = new BufferedReader(fr);
			line = br.readLine();
			while(line != null) {
				
				linecont++;
				charcont = charcont + line.length();
			}
			System.out.println("File : " + filee + " has " + linecont + " lines and "+ charcont +" characters");
			} catch (IOException e) {
				System.out.println(e.getMessage());
			} finally {
				if (fr != null) {
					fr.close();
				}
			}

		}
	}
	
	
}
package streams;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;


public class Ex7 {
	
	public static void main(String[] args) throws IOException {
		File file = new File("dump/file1.txt");
		HashMap<Character, Integer> characters = new HashMap<Character, Integer>();
		String line;
		
		int reps;
		int chars = 0;
		
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			while ((line = br.readLine()) != null) {
				for (int i = 0; i < line.length(); i++) {
					
					if (characters.get(line.charAt(i)) == null) {
						reps = 0;
					} else {
						reps = characters.get(line.charAt(i));
					}
					characters.put(line.charAt(i), reps + 1);
				}
				chars = chars + line.length();
			}
			
			fr.close();
		
		FileWriter fw = new FileWriter("dump/file1output.txt", true);
		PrintWriter pw = new PrintWriter(fw, true);

		for (Character key : characters.keySet()) {
			
		  
		   pw.println("Letter " + key + " has been found " + characters.get(key) + " times with a percentage of " +  ((float)characters.get(key) / chars * 100) + "%");
		   
		}

		fw.close();
			
		
		
	}
	
}


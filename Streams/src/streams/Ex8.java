package streams;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ex8 {
	public static void main(String[] args) throws IOException {
		
	
		String line;
		
		String url;
		
		
		URL u = new URL("https://docs.oracle.com/javase/7/docs/api/java/net/URL.html");
		URLConnection urlcon = u.openConnection();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				urlcon.getInputStream()));
		
		FileWriter fw = new FileWriter("dump/urls.txt", true);
		PrintWriter pw = new PrintWriter(fw, true);
		Pattern p = Pattern.compile("href=\"(.*?)\"");
		Matcher matcher;
		while ((line = br.readLine()) != null) {
		matcher = p.matcher(line);
		
		if (matcher.find()) {
		    url = matcher.group(1); // this variable should contain the link URL
		    pw.println(url);
		}
		
		}
		
		br.close();
		fw.close();
	}


}

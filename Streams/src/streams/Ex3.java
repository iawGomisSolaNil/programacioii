package streams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Ex3 {

	public static void main(String[] args) throws IOException {
		File file1 = new File("dump/file1.txt");
		File file2 = new File("dump/file2.txt");
		FileReader fr1 = null;
		FileReader fr2 = null;
		BufferedReader br1;
		BufferedReader br2;
		String line1;
		String line2;
		
		fr1 = new FileReader(file1);
		br1 = new BufferedReader(fr1);
		
		fr2 = new FileReader(file2);
		br2 = new BufferedReader(fr2);
		
		int cont = 1;
		line1 = br1.readLine();
		line2 = br2.readLine();
		try {
			
			
			while (line1!=null && line2!=null) {
				
				if (!line1.equals(line2)) {
					System.out.println("The line that differs form file 1 is " + line1);
					System.out.println("The line that differs form file 2 is " + line2);
					System.out.println("They differ on line num: " + cont);
				}
				line1 = br1.readLine();
				line2 = br2.readLine();
				cont++;
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			if (fr1 != null && fr2 != null) {
				fr1.close();
				fr2.close();
			}
		}
	}

}
